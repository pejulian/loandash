# loandash

> a [Sails](http://sailsjs.org) application

node -v 0.10.36
---

After checking out this repo, create your local configuration file at this name and path:

`config/local.js`

Refer [here](http://sailsjs.org/#/documentation/reference/sails.config/sails.config.local.html) for more information on the contents of this file.

---

This project uses jade templating and has bower integration for front end dependencies.
Please ensure that the following node modules are installed globally via `npm install -g`:

* `jade`
* `sails-generate-views-jade`
* `sails-generate-bower`


---

Before lifting the application server, run

* `npm install`
* `bower install`
