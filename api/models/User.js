/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {

  attributes: {

  	firstName: {
  		type: 'string',
  		minLength: 1,
      maxLength: 50
  	},

  	lastName: {
  		type: 'string',
  		minLength: 1,
      maxLength: 50
  	},

  	phoneNumber: {
  		type: 'string',
  		defaultsTo: '+60123456789'
  	},

  	email: {
  		type: 'email',
    	required: true
  	},

  	username: {
  		type: 'string',
  		required: true,
    	unique: true
  	},

  	password: {
  		type: 'string',
  		required: true,
  		minLength: 3
  	},

  	passwordConfirmation: {
  		type: 'string',
  		required: true
  	},

  	// role: {
  	// 	type: 'string',
  	// 	enum: ['superuser', 'admin', 'user'],
  	// 	required: true
  	// },

  	activated: {
  		type: 'boolean',
  		enum: [ true, false ],
  		defaultsTo: false
  	},

		superuser: {
  		type: 'boolean',
  		enum: [ true, false ],
  		defaultsTo: false
  	},

  	admin: {
  		type: 'boolean',
			enum: [ true, false ],
  		defaultsTo: false
  	},

		user: {
  		type: 'boolean',
  		enum: [ true, false ],
  		defaultsTo: true
  	},

  	// Its like overriding to string in Java
  	toJSON: function() {
  		var obj = this.toObject();
  		delete obj.password;
  		delete obj.passwordConfirmation;
  		return obj;
  	},

  	// Custom methods
  	fullName: function() {
  		return this.firstName + ' ' + this.lastName;
  	},

  	role: function() {
  		if ( this.superuser )
  			return 'Superuser';
  		else if ( this.admin )
  			return 'Administrator';
  		else
  			return 'User';
  	}
  },

  // Custom Validation
  types: {

  	password: function( password ) {
  		return password === this.passwordConfirmation;
  	}
  },

  // Validation
  validation: {
     
      username: {
          required: 'Username is required.',
          unique: 'This username is already taken.'
      },

      email: {
          required: 'Email is required!'
      },

      firstName: {
          required: 'First name required.',
          minLength: 'First name must contain at least 1 character!',
          maxLength: 'First name cannot exceed 50 characters!'
      },

      lastName: {
          required: 'Last name required.',
          minLength: 'Last name must contain at 1 character!',
          maxLength: 'Last name cannot exceed 50 characters!'
      },

      password: {
          required: 'Password required.',
          password: 'Passwords do not match!',
          minLength: 'Password should contain at least 3 characters.'
      },

      passwordConfirmation: {
          required: 'Password must be retyped.',
          password: 'Passwords do not match!'
      }
    },

 	// Lifecycle methods
  beforeCreate: function(user, cb) {

    // User role management
    // If user is to be created as admin or superuser, make its user role false
    if ( user.admin || user.superuser ) 
      user.user = false;

    // hash the password and delete passwordConfirmation so that it
    // is not saved into the table

    delete user.passwordConfirmation;

    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          console.log(err);
          cb(err);
        }
        else {
          user.password = hash;
          cb(null, user);
        }
      });
    });
  }

};

