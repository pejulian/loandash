/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var passport = require('passport');

module.exports = {
	
	login: function( req, res ) {

    req.session.flash = {};

    // Dont show this page if already logged in 
		if ( req.isAuthenticated() ) {
			return res.redirect('/dashboard');
		}

		if ( req.method === 'GET' ) {

			return res.view({
				title: 'Login'
			});
		}
		else if ( req.method === 'POST' ) {

			passport.authenticate('local', function( err, user, info ) {

				if ( err || !user ) {
					
					sails.log.error( info );
					req.session.flash.err = info;
					return res.redirect('/dashboard/auth/login');
				}

				req.logIn( user, function( err ) {

					if ( err ) {
						sails.log.error( err );
						return res.send( err );
					}

					return res.redirect('/dashboard');

				});

			})( req, res );
		}
	},


	logout: function( req, res ) {
		req.logout();
		return res.redirect('/dashboard/auth/login');
	}
	
};

