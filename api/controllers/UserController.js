/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var lodash = require('lodash'),
		passport = require('passport'),
		async = require('async');

module.exports = {

	create: function( req, res, next ) {

		req.session.flash = {};

		if (req.method === 'GET') {

			var params = {
				title: 'Create Account'
			};

			if ( req.user ) {
				params.isAdmin = ( req.user[0].admin | req.user[0].superuser );
			}

			return res.view( params );
		}
		else if (req.method === 'POST') { 

			User.findOneByUsername( req.param('username') ).exec( function( err, user ) {

				if ( err ) {
					sails.log.error( err );
					req.session.flash = {
						err: err
					};
					return res.redirect('/dashboard/user/create');
				}

				if ( user ) {

					req.session.flash = {
						err: {
							username: [lodash.template('User with username "<%= username %>" already exists!')({ username: req.params.all().username })]
						}
					};

					return res.redirect('/dashboard/user/create');
				}

				User.create( req.params.all() ).exec( function ( err, user ) {
					if ( err ) {
						sails.log.error( err );
						if ( err.ValidationError )
							req.session.flash.err = validation( User, err.ValidationError );
						else
							req.session.flash = {
								err: err
							};
						return res.redirect('/dashboard/user/create');
					}
					else {
						return res.redirect('/dashboard/auth/login');
					}
				});
			});
		}
	},

	update: function( req, res ) {

		req.session.flash = {};

		if (req.method === 'GET') {

			var id = req.param('id');

			if ( lodash.isEmpty( id ) || lodash.isNull( id ) ) {
				req.session.flash.err = {
					message: 'Missing user id'
				};
				return res.redirect('/dashboard/user/list');
			}

			User.findOneById( id ).exec( function( err, user ) {

				if ( err ) {
					sails.log.error( err );
					req.session.flash.err = {
						message: [lodash.template('Error looking up user with id <%= id %>')({ id: id })]
					};
					return res.redirect('/dashboard/user/list');
				}

				if ( !user ) {
					req.session.flash.err = {
						message: [lodash.template('User id <%= id %> not found.')({ id: id })]
					};
					return res.redirect('/dashboard/user/list');
				}

				var params = {
					title: 'Edit Account',
					user: user
				};

				if ( req.user ) {
					params.isAdmin = ( req.user[0].admin | req.user[0].superuser );
				}

				return res.view( params );

			});
		}
		else if ( req.method === 'POST' ) {


			// User.update({ id: user.id })

		}
	},

	remove: function( req, res ) {

		req.session.flash = {};

		if (req.method === 'GET') {

			var id = req.param('id');

			if ( lodash.isEmpty( id ) || lodash.isNull( id ) ) {
				req.session.flash.err = {
					message: 'Missing user id'
				};
				return res.redirect('/dashboard/user/list');
			}

			User.destroy({ id: id }).exec( function( err, user ) {

				if ( err ) {
					sails.log.error( err );
					req.session.flash.err = {
						message: [lodash.template('Could not remove user with id <%= id %>')({ id: id })]
					};
					return res.redirect('/dashboard/user/list');
				}
				
				if ( !user ) {
					req.session.flash.err = {
						message: [lodash.template('User id <%= id %> not found.')({ id: id })]
					};
					return res.redirect('/dashboard/user/list');
				}

				req.session.flash.info = {
					message: 'User successfully removed.'
				};	
				return res.redirect('/dashboard/user/list');

			});
		}
	},

	list: function ( req, res ) {

		// Can sort by full name ASC, DESC
		// Can sort by username ASC, DESC
		// Paginated, one page is limit 15, skip 15

		var current = req.param('current') || 0,
				limit = req.param('limit') || 3,
				sort = req.param('sort') || 'username',
				order = req.param('order') || 'asc';

		current = ( lodash.isNaN( current ) ? 0 : parseInt( current ));
		limit = ( lodash.isNaN( limit ) ? 0 : parseInt( limit ));

		if ( lodash.isEmpty( sort ) )
			sort = 'username';

		if ( lodash.isEmpty( order ) )
			order = 'asc';

		var actions = {
			pagination: function( cb ) {

				User.count().exec( function( err, count ) {

					if ( err )
						cb ( err );
					else {
						var pages = [],
								ctr = Math.ceil( count / limit );
            for ( var i = 0, il = ctr; i < il; i++ )
              pages.push( i );
						cb( null, { pages: pages, count: count });
					}
				});
			},
			users: function( cb ) {

				if ( req.param('order') )	// if order was passed as a param
					if ( req.param('order') === 'asc' ) // and the value passed was asc
						order = 'desc';	// reverse it to desc
					else	// or the value passed was desc
						order = 'asc';	// reverse it to asc

				User.find().sort({ sort: order }).limit( limit ).skip ( current * limit ).exec( function( err, users ) {
					if ( err )
						cb ( err )
					else {
						cb( null, users );
					}
				});
			}
		};

		async.parallel( actions, function( err, output ) {

			if ( err ) {
				sails.log.error( err );
				req.session.flash = {
					err: {
						message: 'An error occured while listing users.'
					}
				};
			} 

			return res.view({
				title: 'User List',
				navigation: 'user.list',
				users: output.users,
				total: output.pagination.count,
				pages: output.pagination.pages,
				current: current,
				limit: limit,
				sort: sort,
				order: order
			});

		});
	}
};

