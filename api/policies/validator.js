var lodash = require('lodash');

// Middleware functions that run before controllers to handle flash errors
module.exports = function( req, res, next ) {

	res.locals.flash = {};

	if ( !req.session.flash ) 
		return next();

	res.locals.flash = lodash.clone( req.session.flash );

	// clear flash
	req.session.flash = {};

	next();
};