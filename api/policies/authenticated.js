// api/policies/authenticated.js

module.exports = function( req, res, next ) {

	sails.log( 'Authentication policy running ...' );

	if ( req.isAuthenticated() )
		return next();
	else {

		// return res.session.flash
		req.session.flash = {};
		req.session.flash.err = { message: 'Please login to continue.'};

		return res.redirect('/dashboard/auth/login');

		// return res.send( 403, {
		// 	status: 403,
		// 	message: 'Not authorized. Please login!'
		// });
	}

};