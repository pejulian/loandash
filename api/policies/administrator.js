// api/policies/adminstrator.js

module.exports = function( req, res, next ) {

	sails.log( 'Adminstrator policy running ...' );

	if ( req.session.User && req.session.User.adminstrator ) {
		return next();
	}
	else {

		var requireAdminError = [{name: 'requireAdminError', message: 'You must be an adminstrator to perform this action!'}];

		req.session.flash = {
			err: requireAdminError
		};

	}

};