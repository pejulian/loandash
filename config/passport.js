var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy;

module.exports = {

	http: {

		customMiddleware: function( app ) {

			sails.log('Configuring Express middleware for passport ...');
			app.use( passport.initialize() );
			app.use( passport.session() );
		}
	}

};